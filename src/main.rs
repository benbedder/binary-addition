use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::sync::{Arc, mpsc, Mutex};
use std::thread;

use clap::{ArgMatches, Command};
use rand::Rng;

fn main() {
    let matches = Command::new("binary-addition")
        .about("A simple utility to person unsigned binary addition given a file.")
        .version("0.0.1")
        .subcommand_required(true)
        .arg_required_else_help(true)
        .subcommand(
            Command::new("generate")
                .short_flag('g')
                .long_flag("generate")
                .about("Generate test file with correct test results."),
        )
        .subcommand(
            Command::new("process")
                .short_flag('p')
                .long_flag("process")
                .about("Process the generated file."),
        )
        .get_matches();
    handle_cli_matches(&matches);
}

fn handle_cli_matches(matches: &ArgMatches) {
    match matches.subcommand() {
        Some(("generate", _)) => {
            match File::create("addition.txt") {
                Ok(f) => {
                    let mut buf_writer = BufWriter::new(f);
                    for _ in 0..1000000 {
                        let mut generator = rand::thread_rng();
                        let u_1: i32 = generator.gen();
                        let u_2: i32 = generator.gen();
                        let (u_res, _) = u_1.overflowing_add(u_2);
                        buf_writer
                            .write_all(format!("{}\n{} {}\n", u_1, u_2, u_res).as_bytes())
                            .unwrap();
                    }
                    buf_writer.flush().unwrap();
                }
                _ => todo!("implement this later"),
            };
        }
        Some(("process", _)) => match OpenOptions::new().read(true).open("addition.txt") {
            Ok(f) => {
                let (tx, rx) = mpsc::channel();
                let send = Arc::new(Mutex::new(tx));
                let receive = Arc::new(Mutex::new(rx));
                let mut buf_read = BufReader::new(f);

                let thread_one = thread::spawn(move || {
                    let shared_mutex = Arc::clone(&send);
                    let mut s = String::new();
                    loop {
                        match buf_read.read_line(&mut s) {
                            Ok(0) => {
                                shared_mutex.lock().unwrap().send(String::from("")).unwrap();
                                break;
                            }
                            Ok(_) => {
                                s = s.trim().parse().unwrap();
                                shared_mutex.lock().unwrap().send(s.clone()).unwrap();
                                s.clear();
                            },
                            _ => println!("boom!"),
                        }
                    }
                });

                let thread_two = thread::spawn(move || {
                    let shared_received_mutex = Arc::clone(&receive);
                    loop {
                        let receiver_channel = shared_received_mutex.lock().unwrap();
                        let res = receiver_channel.recv().unwrap();
                        if res.eq_ignore_ascii_case("") {
                            break;
                        } else {
                            println!("{}", res);
                        }
                    }
                });

                thread_one.join().unwrap();
                thread_two.join().unwrap();
            }
            _ => println!("failed to read the file"),
        },
        _ => println!("generate doesnt match"),
    }
}
